<?php

/**
 * @file
 * Bartik's theme implementation to display a single Drupal page.
 *
 * The doctype, html, head and body tags are not in this template. Instead they
 * can be found in the html.tpl.php template normally located in the
 * modules/system directory.
 *
 * Available variables:
 *
 * General utility variables:
 * - $base_path: The base URL path of the Drupal installation. At the very
 *   least, this will always default to /.
 * - $directory: The directory the template is located in, e.g. modules/system
 *   or themes/jtg.
 * - $is_front: TRUE if the current page is the front page.
 * - $logged_in: TRUE if the user is registered and signed in.
 * - $is_admin: TRUE if the user has permission to access administration pages.
 *
 * Site identity:
 * - $front_page: The URL of the front page. Use this instead of $base_path,
 *   when linking to the front page. This includes the language domain or
 *   prefix.
 * - $logo: The path to the logo image, as defined in theme configuration.
 * - $site_name: The name of the site, empty when display has been disabled
 *   in theme settings.
 * - $site_slogan: The slogan of the site, empty when display has been disabled
 *   in theme settings.
 * - $hide_site_name: TRUE if the site name has been toggled off on the theme
 *   settings page. If hidden, the "element-invisible" class is added to make
 *   the site name visually hidden, but still accessible.
 * - $hide_site_slogan: TRUE if the site slogan has been toggled off on the
 *   theme settings page. If hidden, the "element-invisible" class is added to
 *   make the site slogan visually hidden, but still accessible.
 *
 * Navigation:
 * - $main_menu (array): An array containing the Main menu links for the
 *   site, if they have been configured.
 * - $secondary_menu (array): An array containing the Secondary menu links for
 *   the site, if they have been configured.
 * - $breadcrumb: The breadcrumb trail for the current page.
 *
 * Page content (in order of occurrence in the default page.tpl.php):
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title: The page title, for use in the actual HTML content.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 * - $messages: HTML for status and error messages. Should be displayed
 *   prominently.
 * - $tabs (array): Tabs linking to any sub-pages beneath the current page
 *   (e.g., the view and edit tabs when displaying a node).
 * - $action_links (array): Actions local to the page, such as 'Add menu' on the
 *   menu administration interface.
 * - $feed_icons: A string of all feed icons for the current page.
 * - $node: The node object, if there is an automatically-loaded node
 *   associated with the page, and the node ID is the second argument
 *   in the page's path (e.g. node/12345 and node/12345/revisions, but not
 *   comment/reply/12345).
 *
 * Regions:
 * - $page['header']: Items for the header region.
 * - $page['featured']: Items for the featured region.
 * - $page['highlighted']: Items for the highlighted content region.
 * - $page['help']: Dynamic help text, mostly for admin pages.
 * - $page['content']: The main content of the current page.
 * - $page['sidebar_first']: Items for the first sidebar.
 * - $page['triptych_first']: Items for the first triptych.
 * - $page['triptych_middle']: Items for the middle triptych.
 * - $page['triptych_last']: Items for the last triptych.
 * - $page['footer_firstcolumn']: Items for the first footer column.
 * - $page['footer_secondcolumn']: Items for the second footer column.
 * - $page['footer_thirdcolumn']: Items for the third footer column.
 * - $page['footer_fourthcolumn']: Items for the fourth footer column.
 * - $page['footer']: Items for the footer region.
 *
 * @see template_preprocess()
 * @see template_preprocess_page()
 * @see template_process()
 * @see jtg_process_page()
 * @see html.tpl.php
 */
?>
<main>
<style>
#top {
background: #01d486;
background: linear-gradient(128deg, #01d486 0%, #3daee8 100%);
text-shadow: 0 0 0px #fff;
color: white;
padding-bottom: 100px;
}
.block-nav, .shadow {
  display: block;
  position: relative
}

.block-nav {
  top: -60px;
  padding: 10px;
  max-width: 400px;
  text-align: center;
  background-color: white; }

a.block-nav:hover, a.shadow:hover {
  transform: translateY(-1vh);
  box-shadow: 0 11px 15px -7px rgba(0, 0, 0, 0.2), 0 24px 38px 3px rgba(0, 0, 0, 0.14), 0 9px 46px 8px rgba(0, 0, 0, 0.12); }

.block-nav *, .block-nav *:hover {
  color: black; }

.block-nav i {
  font-size: 3em; }
.tool, .block-nav {
  background: #FFFFFF;
  border: 1px solid rgba(0, 0, 0, 0.1);
  box-sizing: border-box;
  box-shadow: 0px 2px 7px rgba(0, 0, 0, 0.15);
  border-radius: 3px;
  transition: all 0.3s cubic-bezier(0.25, 0.8, 0.25, 1); }

.tool:hover, .block-nav:hover {
  border: 1px solid rgba(0, 0, 0, 0.1);
  box-sizing: border-box;
  box-shadow: 0px 4px 7px rgba(0, 0, 0, 0.25);
  border-radius: 3px;
}
@media (prefers-color-scheme: dark) {
  .block-nav {
    background-color: #212020;
  }
  .card-body {
    color: white;
  }
}
.fundraising {
  background-color: #fdb9b9;
  width:100%;
  position:sticky;
  top:0;
  left:0;
  z-index:998;
  padding:.1rem;
  backdrop-filter:blur(30px)saturate(90%)
}
.fundraising h2 {
  font-size:3rem;
  margin-top:.3rem;
  margin-bottom:0
}
</style>
  <section class="fundraising"><div class="container"><div class="d-flex align-items-center flex-wrap"><h2 class="mt-3">This website is deprecated, to do a donation go to <a href="https://kde.org/community/donations/">kde.org/community/donations/</a></h2></div></div></section>
  <section id="top">
    <a id="main-content"></a>
    <div class="container text-center">
      <h1 style="color: white !important">Donate and support KDE</h1>
      <p style="max-width: 700px" class="h2 mx-auto">
        Help us create software that gives you full freedom, protects your privacy and
	be a part of a great community
      </p>
    </div>
  </section>
  <a class="block-nav mx-auto" href="https://kde.org/community/donations/">
    <h2 class="mt-1 h3">Show your love for KDE by donating 100€/year!</h2>
  </a>

  <section class="container text-center">
    <h2 class="h1 mb-4">Learn about our activities</h2>
    <p style="max-width: 830px" class="mx-auto">
      Your generous donation funds contributor meetings and infrastructure. It keeps our software
      safe for the future and raises awareness for Free Software and Free Culture. Support our
      annual conferences Akademy, Akademy-es, Lakademy and KDE.in.
    <p>
    <div class="row">
      <div class="col-12 col-md-4 mt-4">
	    <a href="https://ev.kde.org/reports/ev-2019/" class="card shadow d-flex flex-column" style="background-image: url('https://ev.kde.org/reports/ev-2019/images/slider/1.jpg') !important; height: 300px; background-size: cover !important;background-repeat: no-repeat !important; background-position: center !important">
	      <h2 class="mt-auto mb-0 p-2" style="color: white; background-color: #000000bb;">2019 report</h2>
	    </a>
      </div>
      <div class="col-12 col-md-4 mt-4">
	    <a href="https://ev.kde.org/reports/ev-2020/" class="card shadow d-flex flex-column" style="background-image: url('https://ev.kde.org/reports/ev-2020/images/slider/1.jpg') !important; height: 300px; background-size: cover !important;background-repeat: no-repeat !important; background-position: center !important">
	      <h2 class="mt-auto mb-0 p-2" style="color: white; background-color: #000000bb;">2020 report</h2>
	    </a>
      </div>
      <div class="col-12 col-md-4 mt-4">
	    <a href="https://ev.kde.org/reports/ev-2021/" class="card shadow d-flex flex-column" style="background-image: url('https://ev.kde.org/reports/ev-2021/images/slider/1.jpg') !important; height: 300px; background-size: cover !important;background-repeat: no-repeat !important; background-position: center !important">
	      <h2 class="mt-auto mb-0 p-2" style="color: white; background-color: #000000bb;">2021 report</h2>
	    </a>
      </div>
    </div>
    <div class="text-center mt-4"><a href="https://kde.org" class="button">Learn more about KDE</a></div>
  </section>

  <section class="container">
    <?php if ($page['highlighted']): ?><div id="highlighted"><?php print render($page['highlighted']); ?></div><?php endif; ?>
    <div class="text-center">
      <h2 class="h1 text-center mx-auto">Your benefits</h2>
      <p class="mx-auto" style="max-width: 600px">Invitation to attend the annual general assembly of KDE e.V. and get regular first hand reports about KDE's activities</p>
    </div>
  </section>

  <section class="section-community section-blue">
    <div class="container text-center">
      <h1 class="mb-3" id="community">Support An Engaged Community</h1>
      <p style="max-width: 800px" class="mx-auto">Our community has developed a wide variety of applications for communication, work, education and entertainment. We have a strong focus on finding innovative solutions to old and new problems, creating a vibrant, open atmosphere for experimentation.</p>
      <div class="text-center mb-5">
        <a href="/civicrm/contribute/transact?reset=1&id=9" class="noblefir ml-2 mr-2">Become a supporting member</a>
      </div>
    </div>
    <div class="community-grid container">
      <figure class="text-center card community-1">
        <picture class="img-fluid">
          <source srcset="https://kde.org/content/people/lakademy-2019x2.jpg 1x, https://kde.org/content/people/lakademy-2019x2.jpg 2x">
          <img src="https://kde.org/content/people/lakademy-2019x2.jpg" alt="LaKademy 2018 group photo" class="img-fluid">
        </picture>
        <figcaption class="card-body">
          <a href="https://lakademy.kde.org/">LaKademy 2018</a> - Salvador, Brazil
        </figcaption>
      </figure>
      <figure class="text-center card community-2">
        <img src="https://kde.org/content/people/plasma-sprint.jpg" alt="Plasma Sprint" class="img-fluid">
        <figcaption class="card-body">
          <a href="https://dot.kde.org/2018/05/14/plasma-sprint-berlin">Plasma Sprint</a> - Berlin
        </figcaption>
      </figure>
      <figure class="text-center card community-3">
        <picture class="w-100">
          <source srcset="https://kde.org/content/people/goal.jpg 1x, https://kde.org/content/people/goal.jpg 2x">
          <img src="https://kde.org/content/people/goal.jpg" alt="KDE goals" class="img-fluid w-100">
        </picture>
        <figcaption class="card-body">
          <a href="https://dot.kde.org/2019/09/07/kde-decides-three-new-challenges-wayland-consistency-and-apps">KDE Goals</a>
        </figcaption>
      </figure>
      <figure class="text-center card community-4">
        <picture class="img-fluid">
          <source srcset="https://kde.org/content/people/kdenlive-sprintx2.jpg 1x, https://kde.org/content/people/kdenlive-sprintx2.jpg 2x">
          <img src="https://kde.org/content/people/kdenlive-sprintx2.jpg" alt="KDenlive Sprint group photo" class="img-fluid">
        </picture>
        <figcaption class="card-body">
          <a href="https://dot.kde.org/2018/05/12/kdenlive-sprint-movie">Kdenlive Sprint</a> - Paris, France
        </figcaption>
      </figure>
      <figure class="text-center card community-5">
        <picture class="img-fluid">
           <source srcset="https://kde.org/content/people/conf-kde-in-2019x2.jpg 1x, https://kde.org/content/people/conf-kde-in-2019x2.jpg 2x">
           <img src="https://kde.org/content/people/conf-kde-in-2019x2.jpg" alt="Conf.KDE.in group photo" class="img-fluid">
        </picture>
        <figcaption class="card-body">
          <a href="https://conf.kde.in">conf.kde.in</a> - Delhi, India
        </figcaption>
      </figure>
      <figure class="text-center card community-7">
        <picture class="img-fluid">
          <source srcset="https://kde.org/content/people/GSoC.jpg 1x, https://kde.org/content/people/GSoCx2.jpg 2x">
          <img src="https://kde.org/content/people/GSoC.jpg" alt="KDE GSoC students group photo" class="img-fluid">
        </picture>
        <figcaption class="card-body">
          <a href="https://community.kde.org/GSoC">Google Summer of Code students</a>
        </figcaption>
      </figure>
      <figure class="text-center card community-8">
        <picture class="img-fluid">
          <source srcset="https://kde.org/content/people/lakademy-2018.jpg 1x, https://kde.org/content/people/lakademy-2018x2.jpg 2x">
          <img src="https://kde.org/content/people/lakademy-2018.jpg" alt="LaKademy 2018 group photo" class="img-fluid w-100">
        </picture>
        <figcaption class="card-body">
          <a href="https://dot.kde.org/2018/10/25/lakademy-2018-celebrates-22-years-kde">Lakademy</a> - Florianopolis, Brazil
        </figcaption>
      </figure>
      <figure class="text-center card community-6">
        <picture class="img-fluid">
          <source srcset="https://kde.org/content/people/kde-connect.jpg 1x, https://kde.org/content/people/kde-connectx2.jpg 2x">
          <img src="https://kde.org/content/people/kde-connect.jpg" alt="KDE Connect Sprint" class="img-fluid">
        </picture>
        <figcaption class="card-body">
          <a href="https://dot.kde.org/2018/05/28/2018-kde-connect-development-sprint">KDE Connect Sprint</a> - Barcelona
        </figcaption>
      </figure>
    </div>
    <div class="text-center">
      <h2 class="text-center mx-auto">About KDE e.V.</h2>
      <p class="mx-auto" style="max-width: 600px">KDE e.V. is a non-profit organization that represents the KDE Project in legal
      and financial matters. Read more on the <a href="https://ev.kde.org">KDE e.V. website</a> and in the <a href="https://ev.kde.org/reports/">Yearly Reports</a>.</p>
    </div>
  </section>

  <?php if ($page['sidebar_second']): ?>
    <aside class="column sidebar">
      <?php print render($page['sidebar_second']); ?>
    </aside>
  <?php endif; ?>
</main>

<?php if ($page['triptych_first'] || $page['triptych_middle'] || $page['triptych_last']): ?>
  <div id="triptych-wrapper"><div id="triptych" class="clearfix">
    <?php print render($page['triptych_first']); ?>
    <?php print render($page['triptych_middle']); ?>
    <?php print render($page['triptych_last']); ?>
  </div></div> <!-- /#triptych, /#triptych-wrapper -->
<?php endif; ?>

<footer>
  <?php if ($main_menu): ?>
    <style>
    /* HACK DRUPAL */
    #kLinks li a.active { color: #2f55ae; font-size: 1.1rem; }
    main::after {
      margin: 40px 10px 10px
      background-color: inherit;
    }
    </style>
    <section id="kLinks" class="container">
      <?php print theme('links__system_main_menu', array(
        'links' => $main_menu,
        'attributes' => array(
          'id' => 'menu',
          'class' => array('links', 'list-unstyled'),
        ),
      )); ?>
    </section>
  <?php endif; ?>

  <?php if ($page['footer_firstcolumn'] || $page['footer_secondcolumn'] || $page['footer_thirdcolumn'] || $page['footer_fourthcolumn']): ?>
    <div id="footer-columns" class="pull-right">
      <?php print render($page['footer_firstcolumn']); ?>
      <?php print render($page['footer_secondcolumn']); ?>
      <?php print render($page['footer_thirdcolumn']); ?>
      <?php print render($page['footer_fourthcolumn']); ?>
    </div>
  <?php endif; ?>
</footer>


<script>
document.addEventListener("DOMContentLoaded", function() {
  const remoteAddr = '<?=$_SERVER['REMOTE_ADDR']?>';
  if(window.location.href == 'https://relate.kde.org/civicrm/contribute/transact?id=5' || window.location.href == 'https://relate.kde.org/user/register') {
    var blacklistXHR = jQuery.ajax({
      url: "https://relate.kde.org/memberimage/blacklist.php?addr="+remoteAddr,
      type: 'GET',
      error: function() { document.location = 'https://relate.kde.org' }
    });
  }
  Drupal.behaviors.jtg = {
      attach: function(context, settings) {
          $('#teaser').coinslider({
              height: 185,
              width: 485,
              effect: "random",
              spw: 12, // squares per width
              sph: 6, // squares per height
              delay: 5000,
              sDelay: 30, // delay beetwen squares in ms
              titleSpeed: 500, // speed of title appereance in ms
              effect: '', // random, swirl, rain, straight
              navigation: false, // prev next and buttons
              links : false, // show images as links
              hoverPause: false
          });
      }
  };

  $('.page-civicrm-profile-search #page-title').appendTo('#info_container').html("Here are the KDE e.V. supporting members who chose to be publicly listed.");
  $('.crm-search-results table tr td:first-child').remove();
  $('.crm-search-results table tr td:last-child').remove();
  $('.crm-search-results table tr.columnheader').remove();
  $('.custom_post_profile-group').insertBefore('#billing-payment-block');
  $('.node-readmore').remove();

  var currentUserId = <?= $logged_in ? $user->uid : -1 ?>;

  $('#memberships>div>table>tbody>tr.columnheader').append('<th></th>');
    jQuery('#memberships>div>table>tbody>tr.even-row').each(function(index, row) {
                if(jQuery(row).find('td')[0].innerHTML.indexOf("upporting") > 0) {
            jQuery(row).append('<td>Get Badge:<br /><a target="_blank" href="https://relate.kde.org/memberimage/index.php/banner/rectangle/'+currentUserId+'">[Default]</a>\
                          <a target="_blank" href="https://relate.kde.org/memberimage/index.php/banner/full/'+currentUserId+'">[Rectangle]</a>\
                          <a target="_blank" href="https://relate.kde.org/memberimage/index.php/banner/signature/'+currentUserId+'">[Forum Signature]</a></td>');
        } else {
            jQuery(row).append('<td>&nbsp;</td>');
        }
    });
        jQuery('#memberships>div>table>tbody>tr.odd-row').each(function(index, row) {
                if(jQuery(row).find('td')[0].innerHTML.indexOf("upporting") > 0) {
                        jQuery(row).append('<td>Get Badge:<br /><a target="_blank" href="https://relate.kde.org/memberimage/index.php/banner/rectangle/'+currentUserId+'">[Default]</a>\
                          <a target="_blank" href="https://relate.kde.org/memberimage/index.php/banner/full/'+currentUserId+'">[Rectangle]</a>\
                          <a target="_blank" href="https://relate.kde.org/memberimage/index.php/banner/signature/'+currentUserId+'">[Forum Signature]</a></td>');
                } else {
                        jQuery(row).append('<td>&nbsp;</td>');
                }
        });
        //New, Current, Grace, Expired, Pending, Cancelled, Deceased, Incomplete - no payment
        if(jQuery(jQuery('#memberships>div>table>tbody>tr.even-row>td')[4]).html() !== null) {
        if(jQuery(jQuery('#memberships>div>table>tbody>tr.even-row>td')[4]).html().indexOf('New') > 1) {
            jQuery(jQuery('#memberships>div>table>tbody>tr.even-row>td')[5]).html('');
        };
        if(jQuery(jQuery('#memberships>div>table>tbody>tr.even-row>td')[4]).html().indexOf('rrent') > 1) {
            jQuery(jQuery('#memberships>div>table>tbody>tr.even-row>td')[5]).html('');
        };
        if(jQuery(jQuery('#memberships>div>table>tbody>tr.even-row>td')[4]).html().indexOf('Grace') > 1) {
            jQuery(jQuery('#memberships>div>table>tbody>tr.even-row>td')[5]).html('');
        };
        if(jQuery(jQuery('#memberships>div>table>tbody>tr.even-row>td')[4]).html().indexOf('Expired') > 1) {
            jQuery(jQuery('#memberships>div>table>tbody>tr.even-row>td')[5]).html('');
        };
        if(jQuery(jQuery('#memberships>div>table>tbody>tr.even-row>td')[4]).html().indexOf('Pending') > 1) {
            jQuery(jQuery('#memberships>div>table>tbody>tr.even-row>td')[5]).html('');
        };
        if(jQuery(jQuery('#memberships>div>table>tbody>tr.even-row>td')[4]).html().indexOf('Cancelled') > 1) {
            jQuery(jQuery('#memberships>div>table>tbody>tr.even-row>td')[5]).html('');
        };
        if(jQuery(jQuery('#memberships>div>table>tbody>tr.even-row>td')[4]).html().indexOf('Deceased') > 1) {
            jQuery(jQuery('#memberships>div>table>tbody>tr.even-row>td')[5]).html('');
        };
        if(jQuery(jQuery('#memberships>div>table>tbody>tr.even-row>td')[4]).html().indexOf('Incomplete') > 1) {
            jQuery(jQuery('#memberships>div>table>tbody>tr.even-row>td')[5]).html('');
        };
    }

    if(jQuery(jQuery('#memberships>div>table>tbody>tr.odd-row>td')[4]).html() !== null) {
    if(jQuery(jQuery('#memberships>div>table>tbody>tr.odd-row>td')[4]).html().indexOf('New') > 1) {
        jQuery(jQuery('#memberships>div>table>tbody>tr.odd-row>td')[5]).html('');
    };
    if(jQuery(jQuery('#memberships>div>table>tbody>tr.odd-row>td')[4]).html().indexOf('rrent') > 1) {
        jQuery(jQuery('#memberships>div>table>tbody>tr.odd-row>td')[5]).html('');
    };
    if(jQuery(jQuery('#memberships>div>table>tbody>tr.odd-row>td')[4]).html().indexOf('Grace') > 1) {
        jQuery(jQuery('#memberships>div>table>tbody>tr.odd-row>td')[5]).html('');
    };
    if(jQuery(jQuery('#memberships>div>table>tbody>tr.odd-row>td')[4]).html().indexOf('Expired') > 1) {
        jQuery(jQuery('#memberships>div>table>tbody>tr.odd-row>td')[5]).html('');
    };
    if(jQuery(jQuery('#memberships>div>table>tbody>tr.odd-row>td')[4]).html().indexOf('Pending') > 1) {
        jQuery(jQuery('#memberships>div>table>tbody>tr.odd-row>td')[5]).html('');
    };
    if(jQuery(jQuery('#memberships>div>table>tbody>tr.odd-row>td')[4]).html().indexOf('Cancelled') > 1) {
        jQuery(jQuery('#memberships>div>table>tbody>tr.odd-row>td')[5]).html('');
    };
    if(jQuery(jQuery('#memberships>div>table>tbody>tr.odd-row>td')[4]).html().indexOf('Deceased') > 1) {
        jQuery(jQuery('#memberships>div>table>tbody>tr.odd-row>td')[5]).html('');
    };
    if(jQuery(jQuery('#memberships>div>table>tbody>tr.odd-row>td')[4]).html().indexOf('Incomplete') > 1) {
        jQuery(jQuery('#memberships>div>table>tbody>tr.odd-row>td')[5]).html('');
    };
}

var fiscalContent = '<tr class="crm-dashboard-civimember">\
      <td>\
        <div class="header-dark">Fiscal Information</div>\
        <div class="view-content">\
        <div id="memberships">\
            <div class="form-item">\
                <p><a href="http://ev.kde.org">KDE e.V.</a> is the non-profit organization behind the KDE community. It is based in Germany and thus donations are <a href="https://ev.kde.org/donations-taxes-de.php">tax deductible in Germany</a>. If you live in another EU country your donation might also be tax deductible. Please consult your tax advisor for details.</p>\
            </div>\
        </div>\
    </div>\
  </td>\
 </tr>';

if(window.location.href.match('/civicrm/user') !== null) {
    jQuery('.crm-dashboard-civimember').last().after(fiscalContent);
}

if(window.location.href.match('/contribute/')) {
    jQuery('label[for="CIVICRM_QFID_0_payment_processor"]').html('I want to pay via <a href="https://ev.kde.org/contact.php#bank" target="_blank">BANK TRANSFER</a> or SEPA DIRECT DEBIT');
}
});
</script>

