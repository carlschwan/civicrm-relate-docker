<?php

/**
 * @file
 * Bartik's theme implementation to display a single Drupal page.
 *
 * The doctype, html, head and body tags are not in this template. Instead they
 * can be found in the html.tpl.php template normally located in the
 * modules/system directory.
 *
 * Available variables:
 *
 * General utility variables:
 * - $base_path: The base URL path of the Drupal installation. At the very
 *   least, this will always default to /.
 * - $directory: The directory the template is located in, e.g. modules/system
 *   or themes/jtg.
 * - $is_front: TRUE if the current page is the front page.
 * - $logged_in: TRUE if the user is registered and signed in.
 * - $is_admin: TRUE if the user has permission to access administration pages.
 *
 * Site identity:
 * - $front_page: The URL of the front page. Use this instead of $base_path,
 *   when linking to the front page. This includes the language domain or
 *   prefix.
 * - $logo: The path to the logo image, as defined in theme configuration.
 * - $site_name: The name of the site, empty when display has been disabled
 *   in theme settings.
 * - $site_slogan: The slogan of the site, empty when display has been disabled
 *   in theme settings.
 * - $hide_site_name: TRUE if the site name has been toggled off on the theme
 *   settings page. If hidden, the "element-invisible" class is added to make
 *   the site name visually hidden, but still accessible.
 * - $hide_site_slogan: TRUE if the site slogan has been toggled off on the
 *   theme settings page. If hidden, the "element-invisible" class is added to
 *   make the site slogan visually hidden, but still accessible.
 *
 * Navigation:
 * - $main_menu (array): An array containing the Main menu links for the
 *   site, if they have been configured.
 * - $secondary_menu (array): An array containing the Secondary menu links for
 *   the site, if they have been configured.
 * - $breadcrumb: The breadcrumb trail for the current page.
 *
 * Page content (in order of occurrence in the default page.tpl.php):
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title: The page title, for use in the actual HTML content.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 * - $messages: HTML for status and error messages. Should be displayed
 *   prominently.
 * - $tabs (array): Tabs linking to any sub-pages beneath the current page
 *   (e.g., the view and edit tabs when displaying a node).
 * - $action_links (array): Actions local to the page, such as 'Add menu' on the
 *   menu administration interface.
 * - $feed_icons: A string of all feed icons for the current page.
 * - $node: The node object, if there is an automatically-loaded node
 *   associated with the page, and the node ID is the second argument
 *   in the page's path (e.g. node/12345 and node/12345/revisions, but not
 *   comment/reply/12345).
 *
 * Regions:
 * - $page['header']: Items for the header region.
 * - $page['featured']: Items for the featured region.
 * - $page['highlighted']: Items for the highlighted content region.
 * - $page['help']: Dynamic help text, mostly for admin pages.
 * - $page['content']: The main content of the current page.
 * - $page['sidebar_first']: Items for the first sidebar.
 * - $page['triptych_first']: Items for the first triptych.
 * - $page['triptych_middle']: Items for the middle triptych.
 * - $page['triptych_last']: Items for the last triptych.
 * - $page['footer_firstcolumn']: Items for the first footer column.
 * - $page['footer_secondcolumn']: Items for the second footer column.
 * - $page['footer_thirdcolumn']: Items for the third footer column.
 * - $page['footer_fourthcolumn']: Items for the fourth footer column.
 * - $page['footer']: Items for the footer region.
 *
 * @see template_preprocess()
 * @see template_preprocess_page()
 * @see template_process()
 * @see jtg_process_page()
 * @see html.tpl.php
 */
?>
<main class="container">
  <section>
    <?php print render($content['body']['#items'][0]['safe_summary']);?>
    <?php if ($messages): ?>
      <div id="messages"><div class="section clearfix">
        <?php print $messages; ?>
      </div>
    <?php endif; ?>
  </section>

  <?php if ($page['featured']): ?>
  <section id="featured">
    <div class="section clearfix">
      <?php print render($page['featured']); ?>
    </div>
  </section>
  <?php endif; ?>

  <section>
    <?php if ($page['highlighted']): ?><div id="highlighted"><?php print render($page['highlighted']); ?></div><?php endif; ?>
    <a id="main-content"></a>
    <?php print render($title_prefix); ?>
    <?php if ($title): ?>
      <h1 class="title" id="page-title">
        <?php print $title; ?>
      </h1>
    <?php endif; ?>
    <?php print render($title_suffix); ?>
    <?php if ($tabs): ?>
      <div class="tabs">
        <?php print render($tabs); ?>
      </div>
    <?php endif; ?>
    <?php print render($page['help']); ?>
    <?php if ($action_links): ?>
      <ul class="action-links">
        <?php print render($action_links); ?>
      </ul>
    <?php endif; ?>
    <?php print render($page['content']); ?>
    <?php //print $feed_icons; ?>
  </section>

  <?php if ($page['sidebar_second']): ?>
    <aside class="column sidebar">
      <?php print render($page['sidebar_second']); ?>
    </aside>
  <?php endif; ?>
</main>

<?php if ($page['triptych_first'] || $page['triptych_middle'] || $page['triptych_last']): ?>
  <div id="triptych-wrapper"><div id="triptych" class="clearfix">
    <?php print render($page['triptych_first']); ?>
    <?php print render($page['triptych_middle']); ?>
    <?php print render($page['triptych_last']); ?>
  </div></div> <!-- /#triptych, /#triptych-wrapper -->
<?php endif; ?>

<footer>
  <?php if ($main_menu): ?>
    <style>
    /* HACK DRUPAL */
    #kLinks li a.active { color: #2f55ae; font-size: 1.1rem; }
    main::after {
      margin: 40px 10px 10px
      background-color: inherit;
    }
    </style>
    <section id="kLinks" class="container">
      <?php print theme('links__system_main_menu', array(
        'links' => $main_menu,
        'attributes' => array(
          'id' => 'menu',
          'class' => array('links', 'list-unstyled'),
        ),
      )); ?>
    </section>
  <?php endif; ?>

  <?php if ($page['footer_firstcolumn'] || $page['footer_secondcolumn'] || $page['footer_thirdcolumn'] || $page['footer_fourthcolumn']): ?>
    <div id="footer-columns" class="pull-right">
      <?php print render($page['footer_firstcolumn']); ?>
      <?php print render($page['footer_secondcolumn']); ?>
      <?php print render($page['footer_thirdcolumn']); ?>
      <?php print render($page['footer_fourthcolumn']); ?>
    </div>
  <?php endif; ?>
</footer>


<script>
document.addEventListener("DOMContentLoaded", function() {

  document.querySelectorAll('.crm-container input.crm-form-text, .crm-container .crm-icon-picker-button, .crm-container input.dateplugin, .crm-container input.crm-form-password')
    .forEach(el => {
      el.classList.add('form-control');
      el.classList.remove('crm-form-text', 'crm-form-password', 'dateplugin')
    });

  const remoteAddr = '<?=$_SERVER['REMOTE_ADDR']?>';
  if(window.location.href == 'https://relate.kde.org/civicrm/contribute/transact?id=5' || window.location.href == 'https://relate.kde.org/user/register') {
    var blacklistXHR = $.ajax({
      url: "https://relate.kde.org/memberimage/blacklist.php?addr="+remoteAddr,
      type: 'GET',
      error: function() { document.location = 'https://relate.kde.org' }
    });
  }
  Drupal.behaviors.jtg = {
      attach: function(context, settings) {
          $('#teaser').coinslider({
              height: 185,
              width: 485,
              effect: "random",
              spw: 12, // squares per width
              sph: 6, // squares per height
              delay: 5000,
              sDelay: 30, // delay beetwen squares in ms
              titleSpeed: 500, // speed of title appereance in ms
              effect: '', // random, swirl, rain, straight
              navigation: false, // prev next and buttons
              links : false, // show images as links
              hoverPause: false
          });
      }
  };

  $('.page-civicrm-profile-search #page-title').appendTo('#info_container').html("Here are the KDE e.V. supporting members who chose to be publicly listed.");
  $('.crm-search-results table tr td:first-child').remove();
  $('.crm-search-results table tr td:last-child').remove();
  $('.crm-search-results table tr.columnheader').remove();
  $('.custom_post_profile-group').insertBefore('#billing-payment-block');
  $('.node-readmore').remove();

  var currentUserId = <?= $logged_in ? $user->uid : -1 ?>;

  $('#memberships>div>table>tbody>tr.columnheader').append('<th></th>');
    $('#memberships>div>table>tbody>tr.even-row').each(function(index, row) {
                if($(row).find('td')[0].innerHTML.indexOf("upporting") > 0) {
            $(row).append('<td>Get Badge:<br /><a target="_blank" href="https://relate.kde.org/memberimage/index.php/banner/rectangle/'+currentUserId+'">[Default]</a>\
                          <a target="_blank" href="https://relate.kde.org/memberimage/index.php/banner/full/'+currentUserId+'">[Rectangle]</a>\
                          <a target="_blank" href="https://relate.kde.org/memberimage/index.php/banner/signature/'+currentUserId+'">[Forum Signature]</a></td>');
        } else {
            $(row).append('<td>&nbsp;</td>');
        }
    });
        $('#memberships>div>table>tbody>tr.odd-row').each(function(index, row) {
                if($(row).find('td')[0].innerHTML.indexOf("upporting") > 0) {
                        $(row).append('<td>Get Badge:<br /><a target="_blank" href="https://relate.kde.org/memberimage/index.php/banner/rectangle/'+currentUserId+'">[Default]</a>\
                          <a target="_blank" href="https://relate.kde.org/memberimage/index.php/banner/full/'+currentUserId+'">[Rectangle]</a>\
                          <a target="_blank" href="https://relate.kde.org/memberimage/index.php/banner/signature/'+currentUserId+'">[Forum Signature]</a></td>');
                } else {
                        $(row).append('<td>&nbsp;</td>');
                }
        });
        //New, Current, Grace, Expired, Pending, Cancelled, Deceased, Incomplete - no payment
        if($($('#memberships>div>table>tbody>tr.even-row>td')[4]).html() !== null) {
        if($($('#memberships>div>table>tbody>tr.even-row>td')[4]).html().indexOf('New') > 1) {
            $($('#memberships>div>table>tbody>tr.even-row>td')[5]).html('');
        };
        if($($('#memberships>div>table>tbody>tr.even-row>td')[4]).html().indexOf('rrent') > 1) {
            $($('#memberships>div>table>tbody>tr.even-row>td')[5]).html('');
        };
        if($($('#memberships>div>table>tbody>tr.even-row>td')[4]).html().indexOf('Grace') > 1) {
            $($('#memberships>div>table>tbody>tr.even-row>td')[5]).html('');
        };
        if($($('#memberships>div>table>tbody>tr.even-row>td')[4]).html().indexOf('Expired') > 1) {
            $($('#memberships>div>table>tbody>tr.even-row>td')[5]).html('');
        };
        if($($('#memberships>div>table>tbody>tr.even-row>td')[4]).html().indexOf('Pending') > 1) {
            $($('#memberships>div>table>tbody>tr.even-row>td')[5]).html('');
        };
        if($($('#memberships>div>table>tbody>tr.even-row>td')[4]).html().indexOf('Cancelled') > 1) {
            $($('#memberships>div>table>tbody>tr.even-row>td')[5]).html('');
        };
        if($($('#memberships>div>table>tbody>tr.even-row>td')[4]).html().indexOf('Deceased') > 1) {
            $($('#memberships>div>table>tbody>tr.even-row>td')[5]).html('');
        };
        if($($('#memberships>div>table>tbody>tr.even-row>td')[4]).html().indexOf('Incomplete') > 1) {
            $($('#memberships>div>table>tbody>tr.even-row>td')[5]).html('');
        };
    }

    if($($('#memberships>div>table>tbody>tr.odd-row>td')[4]).html() !== null) {
    if($($('#memberships>div>table>tbody>tr.odd-row>td')[4]).html().indexOf('New') > 1) {
        $($('#memberships>div>table>tbody>tr.odd-row>td')[5]).html('');
    };
    if($($('#memberships>div>table>tbody>tr.odd-row>td')[4]).html().indexOf('rrent') > 1) {
        $($('#memberships>div>table>tbody>tr.odd-row>td')[5]).html('');
    };
    if($($('#memberships>div>table>tbody>tr.odd-row>td')[4]).html().indexOf('Grace') > 1) {
        $($('#memberships>div>table>tbody>tr.odd-row>td')[5]).html('');
    };
    if($($('#memberships>div>table>tbody>tr.odd-row>td')[4]).html().indexOf('Expired') > 1) {
        $($('#memberships>div>table>tbody>tr.odd-row>td')[5]).html('');
    };
    if($($('#memberships>div>table>tbody>tr.odd-row>td')[4]).html().indexOf('Pending') > 1) {
        $($('#memberships>div>table>tbody>tr.odd-row>td')[5]).html('');
    };
    if($($('#memberships>div>table>tbody>tr.odd-row>td')[4]).html().indexOf('Cancelled') > 1) {
        $($('#memberships>div>table>tbody>tr.odd-row>td')[5]).html('');
    };
    if($($('#memberships>div>table>tbody>tr.odd-row>td')[4]).html().indexOf('Deceased') > 1) {
        $($('#memberships>div>table>tbody>tr.odd-row>td')[5]).html('');
    };
    if($($('#memberships>div>table>tbody>tr.odd-row>td')[4]).html().indexOf('Incomplete') > 1) {
        $($('#memberships>div>table>tbody>tr.odd-row>td')[5]).html('');
    };
}

var fiscalContent = '<tr class="crm-dashboard-civimember">\
      <td>\
        <div class="header-dark">Fiscal Information</div>\
        <div class="view-content">\
        <div id="memberships">\
            <div class="form-item">\
                <p><a href="http://ev.kde.org">KDE e.V.</a> is the non-profit organization behind the KDE community. It is based in Germany and thus donations are <a href="https://ev.kde.org/donations-taxes-de.php">tax deductible in Germany</a>. If you live in another EU country your donation might also be tax deductible. Please consult your tax advisor for details.</p>\
            </div>\
        </div>\
    </div>\
  </td>\
 </tr>';

if(window.location.href.match('/civicrm/user') !== null) {
    $('.crm-dashboard-civimember').last().after(fiscalContent);
}

if(window.location.href.match('/contribute/')) {
    $('label[for="CIVICRM_QFID_0_payment_processor"]').html('I want to pay via <a href="https://ev.kde.org/contact.php#bank" target="_blank">BANK TRANSFER</a> or SEPA DIRECT DEBIT');
}
});
</script>

